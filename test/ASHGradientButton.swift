//
//  ASHGradientButton.swift
//  test
//
//  Created by Alexander Shmakov on 01.09.17.
//  Copyright © 2017 Alexander Shmakov. All rights reserved.
//

import UIKit

@IBDesignable
class ASHGradientButton: UIButton {
    
    enum Gradient {
        case viceCity, mild
        
        func colors() -> [UIColor] {
            switch self {
            case .viceCity: return [UIColor(hex: "3494e6"), UIColor(hex: "ec6ead")]
            case .mild: return [UIColor(hex: "67B26F"), UIColor(hex: "4ca2cd")]
            }
        }
    }
    
    
    /// layer, which applies to button background. Сhange it if you need another gradient direction, more than 2 colors or something else.
    var gradientLayer: CAGradientLayer?
    
    /// If gradient is set (you can do it programmatically), colors() of it will be applied to gradientLayer in higher priority than colors set in Interface Builder.
    var gradient: Gradient? {
        didSet {
            updateView()
        }
    }
    
    var animationDuration: TimeInterval = 1
    
    @IBInspectable
    public var isAnimated: Bool = true
    
    @IBInspectable
    public var gradientColor1: UIColor = UIColor.white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    public var gradientColor2: UIColor = UIColor.black {
        didSet {
            updateView()
        }
    }
    
    private var shouldStopAnimation = false
    private var initialStartPoint: CGPoint = CGPoint(x: 0.3, y: 1)
    private var initialEndPoint: CGPoint = CGPoint(x: 0.6, y: 1)
    private var startPoint: CGPoint = CGPoint(x: 0.4, y: 1)
    private var endPoint: CGPoint = CGPoint(x: 0.6, y: 1)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.addTarget(self, action: #selector(didTouchDownButton), for: .touchDown)
        self.addTarget(self, action: #selector(didTouchUpInsideButton), for: .touchUpInside)
        self.addTarget(self, action: #selector(didTouchUpInsideButton), for: .touchUpOutside)
        self.startPoint = initialStartPoint
        self.endPoint = initialEndPoint
        updateView()
    }
    
    @objc private func didTouchDownButton() {
        self.animate()
    }
    
    @objc private func didTouchUpInsideButton() {
        self.stopAndResetAnimation()
    }
    
    @objc private func didTouchUpOutsideButton() {
        self.stopAndResetAnimation()
    }
    
    private func updateView() {
        self.layer.cornerRadius = self.bounds.height / 2
        self.clipsToBounds = true
        if let gradient = gradient {
            self.applyGradient(colors: gradient.colors())
            return
        }
        self.applyGradient(colors: [gradientColor1, gradientColor2])
    }
    
    private func applyGradient(colors: [UIColor]) {
        self.applyGradient(colors: colors, startPoint: initialStartPoint, endPoint: initialEndPoint)
    }
    
    private func applyGradient(colors: [UIColor], startPoint:CGPoint, endPoint:CGPoint) {

        if gradientLayer == nil {
            self.gradientLayer = CAGradientLayer()
            self.layer.insertSublayer(self.gradientLayer!, at: 0)
        }
        self.gradientLayer?.frame = self.bounds
        self.gradientLayer?.colors = colors.map { $0.cgColor }
        self.gradientLayer?.startPoint = startPoint
        self.gradientLayer?.endPoint = endPoint
        
    }
    
    private func animate() {
        guard isAnimated else { return }
        self.shouldStopAnimation = false
        self.animateForward()
    }
    
    private func animateForward() {
        guard isAnimated else { return }
        
        CATransaction.begin()
        
        let startPointAnimation = CABasicAnimation(keyPath: "startPoint");
        startPointAnimation.fromValue = startPoint
        startPointAnimation.toValue = CGPoint(x: 0.7, y: 1)
        
        let endPointAnimation = CABasicAnimation(keyPath: "endPoint");
        endPointAnimation.fromValue = endPoint
        endPointAnimation.toValue = CGPoint(x: 1, y: 1)
        
        let animations = CAAnimationGroup()
        animations.animations = [startPointAnimation, endPointAnimation]
        animations.duration = self.animationDuration
        animations.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animations.isRemovedOnCompletion = true
        animations.fillMode = kCAFillModeForwards
        
        CATransaction.setCompletionBlock {
            self.startPoint = startPointAnimation.toValue as! CGPoint
            self.endPoint = endPointAnimation.toValue as! CGPoint
            if !self.shouldStopAnimation {
                self.animateBackward()
            }
        }
        
        self.gradientLayer?.add(animations, forKey: nil)
        
        CATransaction.commit()
    }
    
    private func animateBackward() {
        guard isAnimated else { return }
        
        CATransaction.begin()
        
        let startPointAnimation = CABasicAnimation(keyPath: "startPoint");
        startPointAnimation.fromValue = startPoint
        startPointAnimation.toValue = CGPoint(x: 0.0, y: 1)
        
        let endPointAnimation = CABasicAnimation(keyPath: "endPoint");
        endPointAnimation.fromValue = endPoint
        endPointAnimation.toValue = CGPoint(x: 0.3, y: 1)
        
        let animations = CAAnimationGroup()
        animations.animations = [startPointAnimation, endPointAnimation]
        animations.duration = self.animationDuration
        animations.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animations.isRemovedOnCompletion = true
        animations.fillMode = kCAFillModeForwards
        
        CATransaction.setCompletionBlock {
            self.startPoint = startPointAnimation.toValue as! CGPoint
            self.endPoint = endPointAnimation.toValue as! CGPoint
            if !self.shouldStopAnimation {
                self.animateForward()
            }
        }
        
        self.gradientLayer?.add(animations, forKey: nil)
        
        CATransaction.commit()
        
    }
    
    private func stopAndResetAnimation() {
        self.shouldStopAnimation = true
        self.gradientLayer?.removeAllAnimations()
        self.resetGradient()
    }
    
    private func resetGradient() {
        self.startPoint = initialStartPoint
        self.endPoint = initialStartPoint
    }

}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

