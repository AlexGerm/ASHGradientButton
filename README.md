## Description
Simple 2-colors gradient round button with animation on touch.
Colors are customizable with interface builder.
If you want more customization, you can play with `gradientLayer` property.

## Installation and usage
Clone the repository or downloan the archive. Drag `ASHGradientButton.swift` into your project. 
Create button with Interface Builder and set its class to `ASHGradientButton`. 

## Plans
- [ ] Animation Settings
- [ ] Unlimited Colors Support